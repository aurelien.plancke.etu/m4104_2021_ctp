package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String DISTANCIEL;
    private String poste;
    private String salle;

    // TODO Q2.c
    private SuiviViewModel model;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        // TODO Q1
        super.onViewCreated(view, savedInstanceState);
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;
        // TODO Q2.c
        model = new SuiviViewModel(getActivity().getApplication());
        // TODO Q4
        Spinner spSalle = (Spinner)this.getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner)this.getActivity().findViewById(R.id.spPoste);

        ArrayAdapter arrayAdapterSalle = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter arrayAdapterPostes = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);

        spSalle.setAdapter(arrayAdapterSalle);
        spPoste.setAdapter(arrayAdapterPostes);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView tvLogin = (TextView) view.findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                update();
            }

        });
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spSalle = (Spinner)this.getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner)this.getActivity().findViewById(R.id.spPoste);

        if(spSalle.getSelectedItem().toString().equals(DISTANCIEL)){
            spPoste.setVisibility(View.GONE);
            spPoste.setEnabled(false);
            model.setLocalisation(DISTANCIEL);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(spSalle.getSelectedItem().toString() + " : " + spPoste.getSelectedItem().toString());
        }

    }
    // TODO Q9
}