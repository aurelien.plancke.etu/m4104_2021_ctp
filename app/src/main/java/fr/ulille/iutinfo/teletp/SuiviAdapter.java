package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class SuiviAdapter extends ArrayAdapter/* TODO Q6.a */ {
    // TODO Q6.a
    SuiviViewModel model;
    public SuiviAdapter(Context contexte, int idTextView, SuiviViewModel model){
        super(contexte, idTextView, model.getQuestions());
        this.model = model;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View row = inflater.inflate(R.layout.question_view,parent,false);
        TextView label = (TextView)row.findViewById(R.id.question);
        label.setText(model.getQuestion(position));
        // TODO Q7
        return row;

    }


}
