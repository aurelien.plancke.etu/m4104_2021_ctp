package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private String[] questions;
    private MutableLiveData<Integer> liveNextQuestion;
    private Integer nextQuestion;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<Integer>();
        nextQuestion = 0;
        liveNextQuestion.setValue(nextQuestion);

    }
    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a
    public void initQuestions(Context context){
        String[] tmpListQuestion = context.getResources().getStringArray(R.array.list_questions);
        this.questions = new String[tmpListQuestion.length];
        for (int i = 0; i < this.questions.length; i++) {
            this.questions[i] = tmpListQuestion[i];
        }
    }
    public String getQuestion(int position){
        if(position >= 0 && position < this.questions.length )
            return this.questions[position];
        else
            return "position invalide";
    }

    public LiveData<Integer> getLiveNextQuestion(){
        return liveNextQuestion;
    }
    public void setNextQuestion(Integer nextQuestion){
        liveNextQuestion.setValue(nextQuestion);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Prochaine question : " + nextQuestion, Toast.LENGTH_LONG);
        toast.show();
    }

    public Integer getNextQuestion(){
        return liveNextQuestion.getValue();
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public String[] getQuestions() {
        return questions;
    }
}
